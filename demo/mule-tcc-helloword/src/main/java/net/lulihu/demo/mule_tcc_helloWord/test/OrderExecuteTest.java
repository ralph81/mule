package net.lulihu.demo.mule_tcc_helloWord.test;

import net.lulihu.lock.ConditionLock;
import net.lulihu.lock.OrderExecuteLockKit;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 按序执行锁 测试demo
 * <p>
 * 注意如果允许并发线程数小于需要按序执行的对象数量，则必须将需要先执行的交于多线程队列中，否则将导致死锁
 */
public class OrderExecuteTest {

    public static void main(String[] args) {

        OrderExecuteLockKit executeLockKit = new OrderExecuteLockKit();
        ExecutorService pool = Executors.newFixedThreadPool(3);

        Run run1 = new Run(executeLockKit.getConditionLock(), "1");
        Run run2 = new Run(executeLockKit.getConditionLock(), "2");
        Run run3 = new Run(executeLockKit.getConditionLock(), "3");
        Run run4 = new Run(executeLockKit.getConditionLock(), "4");
        Run run5 = new Run(executeLockKit.getConditionLock(), "5");
        Run run6 = new Run(executeLockKit.getConditionLock(), "6");

        pool.execute(run1);
        pool.execute(run4);
        pool.execute(run3);
        pool.execute(run2);
        pool.execute(run6);
        pool.execute(run5);

    }

    public static class Run implements Runnable {

        private final ConditionLock conditionLockKit;

        private final String mes;

        public Run(ConditionLock conditionLockKit, String mes) {
            this.conditionLockKit = conditionLockKit;
            this.mes = mes;
        }

        @Override
        public void run() {
            try {
                conditionLockKit.getLock();

                System.out.println(mes);

            } finally {
                conditionLockKit.unlock();
            }
        }
    }

}
