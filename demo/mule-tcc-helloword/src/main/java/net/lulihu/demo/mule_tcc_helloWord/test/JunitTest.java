package net.lulihu.demo.mule_tcc_helloWord.test;

import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.MuleTccTransactionBootApplication;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.service.MuleTccBootService;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;
import net.lulihu.mule.tccTransaction.service.coordinator.DefaultTransactionCoordinatorServiceImpl;
import net.lulihu.mule.tccTransaction.service.impl.DefaultMuleTccBootServiceImpl;

import java.util.List;

public class JunitTest {


    public static void main(String[] args) {
        MuleTccConfig config = new MuleTccConfig();
        MuleTccConfig.DbConfig dbConfig = new MuleTccConfig.DbConfig();
        dbConfig.setUrl("jdbc:mysql://127.0.0.1:3306/tcc?useUnicode=true&amp;characterEncoding=utf8");
        dbConfig.setUsername("root");
        dbConfig.setPassword("123456");

        config.setDbConfig(dbConfig);
        config.setApplicationName("mule_tcc_springcloud_demo_order");

        TransactionCoordinatorService coordinatorService = new DefaultTransactionCoordinatorServiceImpl();
        MuleTccBootService muleTccBootService = new DefaultMuleTccBootServiceImpl(coordinatorService);
        MuleTccTransactionBootApplication application = new MuleTccTransactionBootApplication(muleTccBootService, config);
        application.start();

        List<MuleTransaction> transactions = coordinatorService.getAllMuleTransaction(null);


        System.exit(-1);
    }

}
