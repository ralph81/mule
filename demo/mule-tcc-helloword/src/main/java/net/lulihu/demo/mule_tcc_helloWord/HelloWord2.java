package net.lulihu.demo.mule_tcc_helloWord;

import net.lulihu.mule.tccTransaction.MuleTccProxyInvocationHandler;
import net.lulihu.mule.tccTransaction.service.impl.DefaultTransactionMethodProxyServiceImpl;

public class HelloWord2 {

    public static void main(String[] args) {
        try {
            HelloWord.start();// 启动

            // 事务代理方法
            DefaultTransactionMethodProxyServiceImpl transactionMethodProxyService = new DefaultTransactionMethodProxyServiceImpl();

            HelloWord.Car carRubber = (HelloWord.Car) MuleTccProxyInvocationHandler.generateProxyObject(transactionMethodProxyService,
                    null, new HelloWord.Rubber());

            HelloWord.Car carTire = (HelloWord.Car) MuleTccProxyInvocationHandler.generateProxyObject(transactionMethodProxyService,
                    null, new HelloWord.Tire(carRubber));

            HelloWord.Car carTrain = (HelloWord.Car) MuleTccProxyInvocationHandler.generateProxyObject(transactionMethodProxyService,
                    null, new HelloWord.Train(carTire));

            int weight = carTrain.weight();
            System.out.println(weight);
        } finally {
            System.exit(-1);
        }
    }
}
