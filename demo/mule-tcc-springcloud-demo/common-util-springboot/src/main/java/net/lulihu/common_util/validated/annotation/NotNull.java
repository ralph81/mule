package net.lulihu.common_util.validated.annotation;

import net.lulihu.common_util.validated.spring.NotNullValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 参数不可以为空
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = NotNullValidator.class)
public @interface NotNull {

    /**
     * 抛出参数异常，异常信息
     * <p>
     * 空或者null 则抛出默认异常信息
     */
    String message() default "参数不能为空";


    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
