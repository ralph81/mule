package net.lulihu.common_util.validated.spring;

import net.lulihu.common_util.validated.annotation.StringRange;

import javax.validation.ConstraintValidator;

/**
 * 字符串范围验证器
 */
public class StringRangeValidator extends Validator<String> implements ConstraintValidator<StringRange, String> {

    @Override
    public void initialize(StringRange annotation) {
        setAnnotation(annotation);
    }
}
