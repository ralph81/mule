package net.lulihu.common_util.validated.spring;

import net.lulihu.common_util.validated.AnnotationValidated;
import net.lulihu.exception.ParamResolveException;

import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;

/**
 * 参数验证器
 *
 * @param <T>
 */
public abstract class Validator<T> {

    private Annotation annotation;

    public boolean isValid(T value, ConstraintValidatorContext context) {
        try {
            AnnotationValidated.examine(annotation, value, new ParamResolveException());
        } catch (ParamResolveException e) {
            return false;
        }
        return true;
    }

    public void setAnnotation(Annotation annotation) {
        this.annotation = annotation;
    }
}
