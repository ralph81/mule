
package net.lulihu.common_util.controller_result;

import java.lang.annotation.*;

/**
 * 保存原来控制层的返回值 不进行封装
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OriginalControllerReturnValue {
}
