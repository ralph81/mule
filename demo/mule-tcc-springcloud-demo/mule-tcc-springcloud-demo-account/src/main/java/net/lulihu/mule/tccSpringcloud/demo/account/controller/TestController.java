package net.lulihu.mule.tccSpringcloud.demo.account.controller;

import net.lulihu.mule.tccSpringcloud.demo.account.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("test01")
    public Object test01() {
        return testService.test01();
    }


}
