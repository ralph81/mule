package net.lulihu.mule.tccSpringcloud.demo.account.service.impl;

import net.lulihu.mule.tccSpringcloud.demo.account.service.TestService;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {


    @MuleTcc(confirmMethod = "test01Confirm", cancelMethod = "test01Cancel")
    @Override
    public Object test01() {
        return "成功";
    }


    public void test01Confirm() {
        System.out.println("成功");
    }

    public void test01Cancel() {
        System.out.println("失败");
    }

}
