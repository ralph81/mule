package net.lulihu.mule.tccSpringcloud.demo.inventory;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.IDGeneratorKit;
import net.lulihu.ObjectKit.NumberKit;
import net.lulihu.mule.tccSpringcloud.demo.inventory.dao.InventoryMapper;
import net.lulihu.mule.tccSpringcloud.demo.inventory.model.pojo.Inventory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@WebAppConfiguration
@SpringBootTest(classes = InventoryApplication.class)
public class InventoryApplicationTest {

    @Autowired
    private InventoryMapper inventoryMapper;

    /**
     * 生成商品库存
     */
    @Test
    public void generateMerchandiseInventory() {
        int productCount = 1000;//生成商品数量
        int[] productQuantity = new int[]{1000, 5000}; // 商品库存取值范围
        int[] productPrice = new int[]{50, 300}; // 商品价格取值范围

        List<Inventory> inventories = new ArrayList<>();
        for (int i = 0; i < productCount; i++) {
            Inventory inventory = new Inventory();
            inventory.setProductNumber(IDGeneratorKit.getStr());
            inventory.setQuantity(NumberKit.rangeRandomNumber(productQuantity[0], productQuantity[1]));
            inventory.setPrice((double) NumberKit.rangeRandomNumber(productPrice[0], productPrice[1]));
            inventories.add(inventory);
        }
        inventoryMapper.batchSave(inventories);
    }


}