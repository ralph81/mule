package net.lulihu.mule.tccSpringcloud.demo.inventory.dao;

import net.lulihu.mule.tccSpringcloud.demo.inventory.model.pojo.Inventory;
import net.lulihu.mule.tccSpringcloud.demo.inventory.model.vo.Product;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface InventoryMapper {

    /**
     * 库存减少
     *
     * @param product 商品信息
     * @return 修改数量
     */
    int decreaseStock(Product product);

    /**
     * 根据商品编号批量获取对应商品库存
     *
     * @param productNumbers 商品编号
     * @return 库存
     */
    List<Inventory> batchGetProductInventory(@Param("productNumbers") String productNumbers);

    /**
     * 批量保存
     */
    int batchSave(@Param("inventories") List<Inventory> inventories);

    List<Inventory> getProductList();

}
