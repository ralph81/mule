package net.lulihu.mule.tccSpringcloud.demo.inventory.config;

import net.lulihu.common_util.exception.CloudExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * 控制器自定义异常处理
 */
@ControllerAdvice
public class ControllerExceptionHandler extends CloudExceptionHandler {
}
