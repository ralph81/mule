package net.lulihu.mule.tccSpringcloud.demo.inventory.service;

import net.lulihu.mule.tccSpringcloud.demo.inventory.model.pojo.Inventory;
import net.lulihu.mule.tccSpringcloud.demo.inventory.model.vo.Product;

import java.util.List;

public interface PressureTestService {

    /**
     * 获取商品库存
     *
     * @param products 商品
     * @return 返回商品信息
     */
    List<Inventory> getProductInventory(List<Product> products);

    /**
     * 获取商品列表
     */
    List<Inventory> getProductList();

}
