package net.lulihu.common_util.exception;

import com.netflix.client.ClientException;
import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 云异常控制
 */
@Slf4j
public class CloudExceptionHandler extends BusinessExceptionHandler {

    /**
     * 服务不可用
     */
    @ExceptionHandler({ClientException.class})
    public Object serviceIsNotAvailable(ClientException e) {
        log.warn("", e);
        return unifiedReturn(BusinessExceptionEnum.SERVICE_IS_NOT_AVAILABLE);
    }

    /**
     * feign 请求超时
     */
    @ExceptionHandler({RetryableException.class})
    public Object nestedServletException(RetryableException e) {
        log.warn("", e);
        return unifiedReturn(BusinessExceptionEnum.EXPECTED_TIMED_ERROR);
    }
}
