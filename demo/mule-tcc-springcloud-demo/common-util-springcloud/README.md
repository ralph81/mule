# common-util-springcloud

#### 介绍
微服务 - springcloud通用工具(包含springboot通用工具->https://gitee.com/lulihu/common-util-springboot)

 为常用的一套spring cloud的客户端整合

#### 最新maven地址
        <dependency>
            <groupId>net.lulihu</groupId>
            <artifactId>common-util-springcloud</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

 
