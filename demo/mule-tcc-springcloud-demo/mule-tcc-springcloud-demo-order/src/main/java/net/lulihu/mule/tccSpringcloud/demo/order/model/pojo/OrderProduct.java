package net.lulihu.mule.tccSpringcloud.demo.order.model.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderProduct {

    /**
     * 主键id
     */
    private Integer pkId;

    /**
     * 订单编号
     */
    private String orderNumber;

    /**
     * 商品编号
     */
    private String productNumber;

    /**
     * 商品数量
     */
    private Integer quantity;

    /**
     * 单价
     */
    private Double price;


}
