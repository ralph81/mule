package net.lulihu.mule.tccSpringcloud.demo.order.service.impl;

import net.lulihu.common_util.controller_result.Result;
import net.lulihu.mule.tccSpringcloud.demo.order.client.AccountClient;
import net.lulihu.mule.tccSpringcloud.demo.order.client.InventoryClient;
import net.lulihu.mule.tccSpringcloud.demo.order.service.OrderService;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private AccountClient accountClient;

    @Autowired
    private InventoryClient inventoryClient;

    @MuleTcc(confirmMethod = "detrimentConfirmMethod", cancelMethod = "detrimentCancelMethod")
    @Override
    public Object detriment(Integer number, Integer unitPrice) {
        Result inventory = inventoryClient.getInventory();
        Integer num = (Integer) inventory.getData();
        if (num < number)
            return "库存不足";

        Result result = accountClient.getBalance();
        Integer balance = (Integer) result.getData();
        if (number * unitPrice > balance)
            return "余额不足";

        return "购买成功";
    }

    public void detrimentConfirmMethod() {
        System.out.println("执行成功");
    }


    public void detrimentCancelMethod() {
        System.out.println("取消回滚");
    }


}
