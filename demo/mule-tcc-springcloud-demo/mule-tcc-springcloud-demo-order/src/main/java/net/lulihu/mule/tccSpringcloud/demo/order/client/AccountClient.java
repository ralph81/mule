package net.lulihu.mule.tccSpringcloud.demo.order.client;

import net.lulihu.common_util.controller_result.Result;
import net.lulihu.mule.tccTransaction.annotation.MuleTcc;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("mule-tcc-springcloud-demo-account")
public interface AccountClient {

    @MuleTcc(currentMethod = true)
    @RequestMapping("getBalance")
    Result getBalance();

}
