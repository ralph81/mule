package net.lulihu.mule.tccSpringcloud.demo.order.dao;

import net.lulihu.mule.tccSpringcloud.demo.order.model.pojo.OrderProduct;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderProductMapper {

    int batchSave(@Param("orderProducts") List<OrderProduct> orderProducts);
}
