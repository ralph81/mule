package net.lulihu.mule.tccSpringcloud;

import com.alibaba.fastjson.JSON;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.mule.tccTransaction.constant.MuleConstant;
import net.lulihu.mule.tccTransaction.kit.TransactionContextLocalKit;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

/**
 * Mule Tcc feign 请求拦截器
 */
@Slf4j
public class MuleTccFeignRequestInterceptor implements RequestInterceptor {


    @Override
    public void apply(RequestTemplate requestTemplate) {
        // 获取事务上下文将其放置请求头中
        TransactionContext transactionContext = TransactionContextLocalKit.get();

        if (null != transactionContext) {
            LogKit.debug(log, "feign请求拦截器，请求服务地址:{}, 事务上下文:{}", () -> new Object[]{requestTemplate.url(), transactionContext});
            requestTemplate.header(MuleConstant.TRANSACTION_CONTEXT, JSON.toJSONString(transactionContext));
        }
    }


}
