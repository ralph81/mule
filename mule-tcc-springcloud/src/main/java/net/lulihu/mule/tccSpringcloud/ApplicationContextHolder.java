package net.lulihu.mule.tccSpringcloud;

import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;
import net.lulihu.mule.tccTransaction.kit.ObjectContextHolder;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNull;


/**
 * 持有应用上下文提供容器服务
 */
public class ApplicationContextHolder implements ApplicationContextAware {

    @Override
    public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {

        ObjectContextHolder.getInstance().setObjectContext(new ObjectContextHolder.ObjectContext() {

            @Override
            public <T> T getBean(Class<T> requiredType) throws NotFindSuitableObjectException {
                try {
                    return applicationContext.getBean(requiredType);
                } catch (BeansException e) {
                    throw new NotFindSuitableObjectException(e);
                }
            }
        });

    }

}
