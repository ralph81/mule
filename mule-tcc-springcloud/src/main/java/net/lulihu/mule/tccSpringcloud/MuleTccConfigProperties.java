package net.lulihu.mule.tccSpringcloud;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "mule-tcc", ignoreInvalidFields = true)
public class MuleTccConfigProperties extends MuleTccConfig {

    /**
     * 为避免springboot做异常拦截器，所以请求结束会从response中获取本次请求状态，
     * 如果请求状态不在当前的设置中，将默认为方法执行错误
     */
    @Getter
    @Setter
    public List<Integer> successCode = new ArrayList<>(Collections.singletonList(200));

}
