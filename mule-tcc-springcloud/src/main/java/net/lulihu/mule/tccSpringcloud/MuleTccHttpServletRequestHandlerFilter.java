package net.lulihu.mule.tccSpringcloud;

import net.lulihu.ObjectKit.ThreadLocalKit;
import net.lulihu.mule.tccTransaction.kit.ServletRequestLocalKit;
import net.lulihu.mule.tccTransaction.kit.StarterTransactionExecutorEventLocalKit;
import net.lulihu.mule.tccTransaction.kit.StarterTransactionHandlerKit;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 请求过滤器
 */
@WebFilter(filterName = "muleTccRequestFilter", urlPatterns = "/*")
public class MuleTccHttpServletRequestHandlerFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    /**
     * 成功返回值http代码
     */
    private final List<Integer> successCode;

    public MuleTccHttpServletRequestHandlerFilter(MuleTccConfigProperties muleTccConfigProperties) {
        this.successCode = muleTccConfigProperties.getSuccessCode();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        // 当前请求放置到当前副本中
        ServletRequestLocalKit.set((HttpServletRequest) request);

        try {
            filterChain.doFilter(request, response);

            // http 状态判断
            int status = ((HttpServletResponse) response).getStatus();
            if (successCode.contains(status))   // 提交事务
                StarterTransactionHandlerKit.exeConfirm(StarterTransactionExecutorEventLocalKit.get());
            else  // 事务取消
                StarterTransactionHandlerKit.exeCancel(StarterTransactionExecutorEventLocalKit.get());
        } catch (Exception e) {
            // 事务取消
            StarterTransactionHandlerKit.exeCancel(StarterTransactionExecutorEventLocalKit.get());
            throw e;
        } finally {
            // 清除当前线程的所有副本信息
            // 避免出现缓存信息未清除导致的意外情况
            ThreadLocalKit.clearAll();
        }
    }

    @Override
    public void destroy() {

    }
}
