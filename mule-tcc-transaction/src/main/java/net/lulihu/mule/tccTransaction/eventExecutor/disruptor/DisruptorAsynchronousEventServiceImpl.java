package net.lulihu.mule.tccTransaction.eventExecutor.disruptor;

import net.lulihu.lock.ConditionLock;
import net.lulihu.ObjectKit.KryoKit;
import net.lulihu.mule.tccTransaction.kit.OrderExecuteLockThreadLocalKit;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

/**
 * 基于 Disruptor 异步执行队列的生产者实现
 */
public class DisruptorAsynchronousEventServiceImpl extends AbstractDisruptorEventPublisher {

    @Override
    public void saveTransaction(MuleTransaction muleTransaction) {
        // 克隆对象防止对象内部存在线程不安全的对象导致数据不一致
        muleTransaction = KryoKit.clone(muleTransaction);
        submit(EventTypeEnum.SAVE, muleTransaction, getConditionLock());
    }

    @Override
    public void updateTransactionParticipant(MuleTransaction muleTransaction) {
        muleTransaction = KryoKit.clone(muleTransaction);
        submit(EventTypeEnum.UPDATE_PARTICIPANT, muleTransaction, getConditionLock());
    }

    @Override
    public void cancel(MuleTransaction muleTransaction, TransactionContext transactionContext) {
        muleTransaction = KryoKit.clone(muleTransaction);
        transactionContext = KryoKit.clone(transactionContext);
        submit(EventTypeEnum.CANCEL, muleTransaction, transactionContext, getConditionLock());
    }

    @Override
    public void confirm(MuleTransaction muleTransaction, TransactionContext transactionContext) {
        muleTransaction = KryoKit.clone(muleTransaction);
        transactionContext = KryoKit.clone(transactionContext);
        submit(EventTypeEnum.CONFIRM, muleTransaction, transactionContext, getConditionLock());
    }

    @Override
    public void rpcConfirm(TransactionContext context) {
        context = KryoKit.clone(context);
        submit(EventTypeEnum.RPC_CONFIRM, context, getConditionLock());
    }

    @Override
    public void rpcCancel(TransactionContext context) {
        context = KryoKit.clone(context);
        submit(EventTypeEnum.RPC_CANCEL, context, getConditionLock());
    }

    @Override
    public void delete(MuleTransaction muleTransaction) {
        muleTransaction = KryoKit.clone(muleTransaction);
        submit(EventTypeEnum.DELETE, muleTransaction, getConditionLock());
    }

    @Override
    public void deleteByContext(TransactionContext context) {
        context = KryoKit.clone(context);
        submit(EventTypeEnum.DELETE_BY_CONTEXT, context, getConditionLock());
    }

    @Override
    public void updateStatus(MuleTransaction muleTransaction) {
        muleTransaction = KryoKit.clone(muleTransaction);
        submit(EventTypeEnum.UPDATE_STATUS, muleTransaction, getConditionLock());
    }

    /**
     * 获取条件锁
     *
     * @return 当前处理线程的条件锁
     */
    private ConditionLock getConditionLock() {
        return OrderExecuteLockThreadLocalKit.get().getConditionLock();
    }

}
