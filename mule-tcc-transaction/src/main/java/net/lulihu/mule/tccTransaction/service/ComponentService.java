package net.lulihu.mule.tccTransaction.service;


/**
 * 构成应用程序的组件顶层接口
 */
public interface ComponentService {

    /**
     * 组件名称
     *
     * @return 仅仅作为打印日志时使用
     */
    default String componentName() {
        return this.getClass().getSimpleName();
    }

}
