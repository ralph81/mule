package net.lulihu.mule.tccTransaction.service.factory;

import net.lulihu.ObjectKit.ClassKit;
import net.lulihu.mule.tccTransaction.service.TransactionExecutorEventService;
import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;
import net.lulihu.mule.tccTransaction.service.TransactionSelfHealingProgramService;

import java.util.*;

/**
 * 事务工厂枚举
 */
enum FactoryComponentEnum {

    TRANSACTION_EXECUTOR("事务执行者", TransactionExecutorEventService.class, new ArrayList<>()),

    TRANSACTION_HANDLER("事务处理器", TransactionHandlerService.class, new ArrayList<>()),

    SELF_HEALING_PROGRAM("自我修改程序", TransactionSelfHealingProgramService.class, new ArrayList<>()),
    ;

    /**
     * 工厂枚举
     *
     * @param description 描述
     * @param clazz       生产元件的抽象接口
     * @param workshop    元件的生产车间
     */
    <T extends TransactionFactoryService> FactoryComponentEnum(String description, Class<T> clazz, Collection<T> workshop) {
        this.description = description;
        this.clazz = clazz;
        this.workshop = workshop;
    }

    /**
     * 根据目标组件类型找到对应的组件集
     *
     * @param targetType 目标组件类型
     * @return 对应的组件集
     * @throws NotFindSuitableObjectException 找不到则抛出异常
     */
    public static FactoryComponentEnum getComponentSetByClazz(Class<?> targetType) throws NotFindSuitableObjectException {
        for (FactoryComponentEnum component : FactoryComponentEnum.values()) {
            if (ClassKit.isAssignable(component.getClazz(), targetType)) {
                return component;
            }
        }
        throw new NotFindSuitableObjectException("目标组件【{}】 未能匹配到对应的组件集...", targetType);
    }

    private final Class<?> clazz;
    private final String description;
    public Collection<?> workshop;

    public String getDescription() {
        return description;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public Collection<?> getWorkshop() {
        return workshop;
    }}
