package net.lulihu.mule.tccTransaction.service;

import net.lulihu.mule.tccTransaction.MuleTccConfig;

/**
 * mule tcc 事务启动服务
 */
public interface MuleTccBootService {

    /**
     * 初始化应用程序
     *
     * @param config 配置
     */
    void initialization(MuleTccConfig config) throws Exception;

    /**
     * 获取应用名称
     */
    String applicationName();


}
