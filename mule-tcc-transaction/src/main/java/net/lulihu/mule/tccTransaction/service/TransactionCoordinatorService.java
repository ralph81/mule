package net.lulihu.mule.tccTransaction.service;

import net.lulihu.lock.ConditionLock;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.MuleTransactionCompensations;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

import java.util.List;

/**
 * 事务协调员服务
 */
public interface TransactionCoordinatorService {

    /**
     * 事务协调器初始化
     *
     * @param repositoryService 协调器储存库
     * @param config            配置
     * @throws Exception 初始化过程中发生任何错误将抛出异常
     */
    void initTransactionCoordinator(TransactionCoordinatorRepositoryService repositoryService, MuleTccConfig config) throws Exception;

    /**
     * 删除事务记录
     *
     * @param muleTransaction 事件记录
     * @param conditionLock   按序执行锁
     */
    void delete(MuleTransaction muleTransaction, ConditionLock conditionLock);

    /**
     * 删除事务记录
     *
     * @param context       事件上下文
     * @param conditionLock 按序执行锁
     */
    void deleteByContext(TransactionContext context, ConditionLock conditionLock);

    /**
     * 事务记录保存
     *
     * @param muleTransaction 事件记录
     * @param conditionLock   按序执行锁
     */
    void save(MuleTransaction muleTransaction, ConditionLock conditionLock);

    /**
     * 保存事务补偿记录
     *
     * @param transactionCompensations 事件补偿记录
     * @param conditionLock            按序执行锁
     */
    void saveCompensationsLog(MuleTransactionCompensations transactionCompensations, ConditionLock conditionLock);

    /**
     * 修改事务参与者信息
     *
     * @param muleTransaction 事件记录
     * @param conditionLock   按序执行锁
     */
    void updateParticipant(MuleTransaction muleTransaction, ConditionLock conditionLock);

    /**
     * 修改事务状态
     *
     * @param muleTransaction 事件记录
     * @param conditionLock   按序执行锁
     */
    void updateStatus(MuleTransaction muleTransaction, ConditionLock conditionLock);

    /**
     * 执行取消方法
     *
     * @param muleTransaction    事务记录
     * @param transactionContext 事务上下文
     * @param conditionLock      按序执行锁
     */
    void cancel(MuleTransaction muleTransaction, TransactionContext transactionContext, ConditionLock conditionLock);

    /**
     * 执行确认方法
     *
     * @param muleTransaction    事务记录
     * @param transactionContext 事务上下文
     * @param conditionLock      按序执行锁
     */
    void confirm(MuleTransaction muleTransaction, TransactionContext transactionContext, ConditionLock conditionLock);

    /**
     * 根据事务id获取事务记录
     *
     * @param transId       事务id
     * @param conditionLock 按序执行锁
     * @return 事务记录
     */
    MuleTransaction getMuleTransactionById(String transId, ConditionLock conditionLock);

    /**
     * rpc 执行成功，通知参与者进行事务确认
     *
     * @param context       事务上下文
     * @param conditionLock 按序执行锁
     */
    void rpcConfirm(TransactionContext context, ConditionLock conditionLock);

    /**
     * rpc 执行失败，通知参与者进行事务取消
     *
     * @param context       事务上下文
     * @param conditionLock 按序执行锁
     */
    void rpcCancel(TransactionContext context, ConditionLock conditionLock);

    /**
     * 获取当前服务所有的事务记录信息
     *
     * @param conditionLock 按序执行锁
     */
    List<MuleTransaction> getAllMuleTransaction(ConditionLock conditionLock);

    /**
     * 获取事务乐观锁
     * <p>
     * 乐观锁由 事务id+最后修改时间+锁版本号一起实现
     * 事务id+锁版本号确定事务锁->最后修改时间确认一段时间内只能获取一次
     *
     * @param transaction         事务信息
     * @param recoverTimeInterval 自动恢复时间间隔
     * @param conditionLock       按序执行锁
     * @return true为获取成功，反之失败
     */
    boolean getOptimisticLocks(MuleTransaction transaction, Integer recoverTimeInterval, ConditionLock conditionLock);

    /**
     * 获取事务补偿记录
     *
     * @param transaction   事务信息
     * @param conditionLock 按序执行锁
     * @return 事务补偿记录
     */
    MuleTransactionCompensations getMuleTransactionCompensationsLog(MuleTransaction transaction, ConditionLock conditionLock);

    /**
     * 删除事务补偿记录
     *
     * @param transactionCompensations 事件补偿记录
     * @param conditionLock            按序执行锁
     */
    void deleteTransactionCompensationsLog(MuleTransactionCompensations transactionCompensations, ConditionLock conditionLock);

    /**
     * 删除多余事务补偿记录
     *
     * @param initialDelay  事务补偿初始延迟时间 单位/秒
     * @param conditionLock 按序执行锁
     */
    void deleteExcessCompensationRecord(int initialDelay, ConditionLock conditionLock);

}
