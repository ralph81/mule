package net.lulihu.mule.tccTransaction.eventExecutor.disruptor;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.lulihu.disruptorKit.EventEndClear;

/**
 * disruptor 事件对象
 */
@Data
@NoArgsConstructor
@ToString
public class DisruptorTransactionEvent implements EventEndClear {

    /**
     * 事件类型
     */
    private EventTypeEnum eventType;

    /**
     * 事件方法参数
     */
    private Object[] args;

    @Override
    public void clear() {
        // 清除方便gc
        this.eventType = null;
        this.args = null;
    }
}
