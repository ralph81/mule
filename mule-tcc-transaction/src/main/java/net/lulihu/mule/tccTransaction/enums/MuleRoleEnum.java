package net.lulihu.mule.tccTransaction.enums;

/**
 * 事务执行过程中的角色
 */
public enum MuleRoleEnum {

    START(1, "事务发起者"),
    START_LOCAL(2, "本地事务参与者"),
    RPC(3, "远端事务参与者"),
    RPC_LOCAL(4, "远端事务本地参与者"),
    CALLBACK_EXECUTION(5, "回调执行"),
    DELETE(6, "删除事务记录"),
    SELF_HEALING(7, "自我修复");


    MuleRoleEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    private int code;

    private String description;

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }}
