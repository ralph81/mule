
package net.lulihu.mule.tccTransaction.serializer;

import net.lulihu.mule.tccTransaction.enums.SerializeEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;
import net.lulihu.ObjectKit.KryoKit;


/**
 * Kryo 序列化
 */
public class KryoSerializer implements ObjectSerializer {

    @Override
    public byte[] serialize(final Object obj) throws MuleTccException {
        return KryoKit.writeToByteArray(obj);
    }

    @Override
    public <T> T deSerialize(final byte[] param) throws MuleTccException {
        return KryoKit.readFromByteArray(param);
    }

    @Override
    public SerializeEnum getScheme() {
        return SerializeEnum.KRYO;
    }
}
