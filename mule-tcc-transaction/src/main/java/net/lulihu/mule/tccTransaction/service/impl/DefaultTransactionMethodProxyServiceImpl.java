package net.lulihu.mule.tccTransaction.service.impl;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.mule.tccTransaction.constant.MuleConstant;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;
import net.lulihu.mule.tccTransaction.kit.OrderExecuteLockThreadLocalKit;
import net.lulihu.mule.tccTransaction.kit.ServletRequestLocalKit;
import net.lulihu.mule.tccTransaction.kit.TransactionContextLocalKit;
import net.lulihu.mule.tccTransaction.kit.TransactionKit;
import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.factory.TransactionComponentFactoryService;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;
import net.lulihu.mule.tccTransaction.service.TransactionMethodProxyService;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryManageEnum;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.ObjectKit.StrKit;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 开启事务的方法代理组件
 */
@Slf4j
public class DefaultTransactionMethodProxyServiceImpl implements TransactionMethodProxyService {

    private final TransactionComponentFactoryService transactionFactoryHandlerService;

    public DefaultTransactionMethodProxyServiceImpl() {
        this.transactionFactoryHandlerService = TransactionFactoryManageEnum.INSTANCE.getTransactionFactoryManage();
    }

    @Override
    public TransactionHandlerService beforeTransactionHandler() throws NotFindSuitableObjectException {
        // 设置当前处理的按序执行锁
        OrderExecuteLockThreadLocalKit.set();
        // 事务上下文判断
        TransactionContext transactionContext = transactionContext();
        // 保存至本地副本
        TransactionContextLocalKit.set(transactionContext);

        // 获取对应的事务处理器
        TransactionHandlerService handler = this.transactionFactoryHandlerService.electionTransactionHandler(transactionContext);
        LogKit.debug(log, "事务上下文:{} - 选举事务处理器:{}", transactionContext, handler.getClass());

        return handler;
    }

    @Override
    public boolean before(TransactionHandlerService handlerService, Class<?> beanClass, Method method, Object[] args) {
        return handlerService.beforeHandler(beanClass, method, args);
    }

    @Override
    public Object result(TransactionHandlerService handlerService, Object result) {
        return handlerService.resultHandler(result);
    }

    @Override
    public Object exception(TransactionHandlerService handlerService, Throwable throwable) throws Throwable {
        return handlerService.exceptionHandler(throwable);
    }

    @Override
    public void after(TransactionHandlerService handlerService) {
        handlerService.afterHandler();
    }

    public TransactionContext transactionContext() {
        // 尝试获取当前的事务上下文
        TransactionContext transactionContext = TransactionContextLocalKit.get();

        if (null != transactionContext) {// 如果可以从本地线程副本中提取事务上下文则表示当前服务为本地参与者
            // 发起者事务本地参与者
            if (transactionContext.getRole() == MuleRoleEnum.START.getCode())
                transactionContext.setRole(MuleRoleEnum.START_LOCAL.getCode());
                // 远端事务本地参与者
            else if (transactionContext.getRole() == MuleRoleEnum.RPC.getCode())
                transactionContext.setRole(MuleRoleEnum.RPC_LOCAL.getCode());
        } else {
            // 获取请求上下文
            HttpServletRequest servletRequest = ServletRequestLocalKit.get();
            String context = (servletRequest == null ? null : servletRequest.getHeader(MuleConstant.TRANSACTION_CONTEXT));
            if (StrKit.notBlank(context)) { // 如果可以从请求中提取事务上下文则表示当前服务为远程参与者
                transactionContext = JSON.parseObject(context, TransactionContext.class);
                transactionContext.setRole(MuleRoleEnum.RPC.getCode());
            } else {
                // 都不匹配则为事务发起者 - 构建事务上下文
                transactionContext = TransactionKit.buildMuleTransactionContext( MuleActionEnum.TRYING, MuleRoleEnum.START);
            }
        }
        return transactionContext;
    }
}
