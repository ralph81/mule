package net.lulihu.mule.tccTransaction.serializer;


import net.lulihu.mule.tccTransaction.enums.SerializeEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;

/**
 * 对象序列化器。
 */
public interface ObjectSerializer {

    /**
     * 序列化对象.
     *
     * @param obj 需要序更列化的对象
     * @return byte []
     * @throws MuleTccException 异常信息
     */
    byte[] serialize(Object obj) throws MuleTccException;


    /**
     * 反序列化对象.
     *
     * @param param 需要反序列化的byte []
     * @param <T>   泛型支持
     * @return 对象
     * @throws MuleTccException 异常信息
     */
    <T> T deSerialize(byte[] param) throws MuleTccException;

    /**
     * 序列化枚举类型
     */
    SerializeEnum getScheme();
}
