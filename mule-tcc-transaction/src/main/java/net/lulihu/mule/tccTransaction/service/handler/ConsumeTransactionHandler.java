package net.lulihu.mule.tccTransaction.service.handler;

import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.model.TransactionContext;


/**
 * 回调执行参与者
 * <p>
 * 既在执行参与者方法时因为需要执行本身的方法被代理过所以会选择处理，该处理器不做任何操作
 */
public class ConsumeTransactionHandler extends AbstractTransactionHandlerService {

    @Override
    public boolean support(TransactionContext transactionContext) {
        int role = transactionContext.getRole();
        return MuleRoleEnum.CALLBACK_EXECUTION.getCode() == role || MuleRoleEnum.SELF_HEALING.getCode() == role;
    }

    @Override
    public String componentName() {
        return "默认事务消费处理器";
    }
}
