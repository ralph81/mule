package net.lulihu.mule.tccTransaction.kit;

import net.lulihu.ObjectKit.ConcurrentThreadLocalKit;
import net.lulihu.mule.tccTransaction.enums.ThreadLocalEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;

import javax.servlet.http.HttpServletRequest;

/**
 * 请求上下文副本
 */
public class ServletRequestLocalKit {

    private static final ConcurrentThreadLocalKit threadLocal = ConcurrentThreadLocalKit.getInstance();

    private static final String key = ThreadLocalEnum.SERVLET_REQUEST.getDescription();

    /**
     * 构造函数私有化
     */
    private ServletRequestLocalKit() {
        if (threadLocal.containsKey(key))
            throw new MuleTccException("当前线程副本中已经存在指定名称【{}】的数据副本", key);

    }

    /**
     * 判断当前事务处理单位是不是请求
     *
     * @return true不是 反之是
     */
    public static boolean notRequest() {
        return get() == null;
    }

    /**
     * 设置本地请求上下文
     *
     * @param httpServletRequest 当前请求上下文
     */
    public static void set(HttpServletRequest httpServletRequest) {
        threadLocal.set(key, httpServletRequest);
    }

    /**
     * 获取本地请求上下文
     */
    public static HttpServletRequest get() {
        return (HttpServletRequest) threadLocal.get(key);
    }

    /**
     * 删除当前线程的请求上下文
     */
    public static void clear() {
        threadLocal.clear(key);
    }
}
