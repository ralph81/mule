package net.lulihu.mule.tccTransaction.kit;

/**
 * 储存库路径工具
 */
public class RepositoryPathKit {

    /**
     * 构建db表名称字符串。
     *
     * @param applicationName 应用名称
     * @return 返回应用程序对应的数据库表名称
     */
    public static String getAppDbTableName(String applicationName) {
        return applicationName.replaceAll("-", "_");
    }

}
