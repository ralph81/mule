package net.lulihu.mule.tccTransaction.banner;

public class MuleTccBanner {

    private static final String defaultBanner = "\n" +
            " ****     **** **     ** **       ********   **********   ******    ****** \n" +
            "/**/**   **/**/**    /**/**      /**/////   /////**///   **////**  **////**\n" +
            "/**//** ** /**/**    /**/**      /**            /**     **    //  **    // \n" +
            "/** //***  /**/**    /**/**      /*******       /**    /**       /**       \n" +
            "/**  //*   /**/**    /**/**      /**////        /**    /**       /**       \n" +
            "/**   /    /**/**    /**/**      /**            /**    //**    **//**    **\n" +
            "/**        /**//******* /********/********      /**     //******  //****** \n" +
            "//         //  ///////  //////// ////////       //       //////    //////  \n";


    public static void banner() {
        String bannerText = buildBannerText();
        System.out.println(bannerText);
    }

    private static String buildBannerText() {
        return defaultBanner;
    }

}
