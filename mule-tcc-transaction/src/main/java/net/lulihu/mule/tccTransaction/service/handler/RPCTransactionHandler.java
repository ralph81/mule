package net.lulihu.mule.tccTransaction.service.handler;

import net.lulihu.ObjectKit.ThreadLocalKit;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.kit.ServletRequestLocalKit;
import net.lulihu.mule.tccTransaction.kit.TransactionContextLocalKit;
import net.lulihu.mule.tccTransaction.kit.TransactionKit;
import net.lulihu.mule.tccTransaction.kit.TransactionLogLocalKit;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;

import java.lang.reflect.Method;

/**
 * RPC 远端参与者事务处理器
 */
public class RPCTransactionHandler extends AbstractTransactionHandlerService {

    @Override
    public boolean support(TransactionContext context) {
        return context.getRole() == MuleRoleEnum.RPC.getCode();
    }

    @Override
    public boolean beforeHandler(Class<?> beanClass, Method method, Object[] args) {
        TransactionContext context = TransactionContextLocalKit.get();

        MuleActionEnum actionEnum = MuleActionEnum.acquire(context.getAction());
        switch (actionEnum) {
            case TRYING:// 方法执行过程中
                // 构建当前服务的事务记录
                MuleTransaction muleTransaction = TransactionKit.buildMuleTransaction(
                        context.getTransId(), MuleRoleEnum.RPC, beanClass, method, args);
                TransactionLogLocalKit.set(muleTransaction);
                // 保存事务记录
                transactionExecutorService.saveTransaction(muleTransaction);
                return true;
            case CONFIRMING: // 确认方法
                context.setRole(MuleRoleEnum.CALLBACK_EXECUTION.getCode());
                transactionExecutorService.rpcConfirm(context);
                break;
            case CANCELING: // 取消方法
                context.setRole(MuleRoleEnum.CALLBACK_EXECUTION.getCode());
                transactionExecutorService.rpcCancel(context);
                break;
            case DELETE: // 删除事务记录
                context.setRole(MuleRoleEnum.DELETE.getCode());
                transactionExecutorService.deleteByContext(context);
                break;
        }
        return false;
    }

    @Override
    public void afterHandler() {
        MuleTransaction muleTransaction = TransactionLogLocalKit.get();
        if (muleTransaction != null) { // 只有try方法记录上下文，这里做个简单判断
            muleTransaction.setStatus(MuleActionEnum.AFTER_TRYING.getCode());
            transactionExecutorService.updateStatus(muleTransaction);
        }
        if (ServletRequestLocalKit.notRequest()) {
            ThreadLocalKit.clearAll();
        }
    }

    @Override
    public String componentName() {
        return "RPC远程参与者处理器";
    }

}
