package net.lulihu.mule.tccTransaction.service.factory;

import net.lulihu.mule.tccTransaction.service.impl.DefaultTransactionComponentFactoryManage;

/**
 * 事务工厂管理枚举
 * <p>
 * 枚举式单例
 */
public enum TransactionFactoryManageEnum {

    INSTANCE,
    ;

    TransactionFactoryManageEnum() {
        this.transactionFactoryManage = new DefaultTransactionComponentFactoryManage();
    }

    private final TransactionComponentFactoryService transactionFactoryManage;

    /**
     * 获取事务工厂组件管理者
     *
     * @return 事务工厂组件管理者
     */
    public TransactionComponentFactoryService getTransactionFactoryManage() {
        return this.transactionFactoryManage;
    }
}