package net.lulihu.mule.tccTransaction.service;

import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryService;

/**
 * 事件执行服务
 */
public interface TransactionExecutorService<T> extends TransactionFactoryService<T> {

    /**
     * 初始化
     *
     * @param config                        配置
     * @param transactionCoordinatorService 事件协调服务
     */
    void initialization(MuleTccConfig config, TransactionCoordinatorService transactionCoordinatorService);


}
