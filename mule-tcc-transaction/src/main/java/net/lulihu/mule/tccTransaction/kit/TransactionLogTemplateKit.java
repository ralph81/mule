package net.lulihu.mule.tccTransaction.kit;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.Assert;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.ReflectKit;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.functional.ConsumerArgsResult;
import net.lulihu.functional.ConsumerResult;
import net.lulihu.mule.tccTransaction.annotation.DbField;
import net.lulihu.mule.tccTransaction.serializer.ObjectSerializer;

import java.lang.reflect.Field;
import java.util.List;

/**
 * 事务记录模板
 */
@Slf4j
public class TransactionLogTemplateKit {

    /**
     * 保存事务记录
     */
    public static final ConsumerResult<String, String> SAVE_TRANSACTION_TEMPLATE =
            tableName -> StrKit.format("INSERT INTO `{}`(`trans_id`, `status`, `role`, `target_class`, `target_method`, `participants`, `version`, `create_time`, `last_time`) VALUES ( ?,?,?,?,?,?,?,?,?,? ) ", tableName);


    /**
     * 修改事务状态模板
     */
    public static final ConsumerResult<String, String> UPDATE_STATUS_TEMPLATE =
            tableName -> StrKit.format("UPDATE `{}` SET `status` = ?, `last_time` = ?  WHERE `trans_id` = ?", tableName);

    /**
     * 修改事务参与者模板
     */
    public static final ConsumerResult<String, String> UPDATE_PARTICIPANT_TEMPLATE =
            tableName -> StrKit.format("UPDATE `{}` SET `participants` = ?, `last_time` = ?  WHERE `trans_id` = ?", tableName);

    /**
     * 根据事务id获取事务信息
     */
    public static final ConsumerResult<String, String> SELECT_TRANSACTION_BY_TRANS_ID_TEMPLATE =
            tableName -> StrKit.format("SELECT `trans_id`, `status`, `role`, `target_class`, `target_method`, `participants`, `version`, `create_time`, `last_time` FROM `{}` WHERE trans_id = ?", tableName);

    /**
     * 根据事务id获取事务补偿信息
     */
    public static final ConsumerResult<String, String> SELECT_TRANSACTION_COMPENSATIONS_LOG_BY_TRANS_ID_TEMPLATE =
            tableName -> StrKit.format("SELECT `trans_id`, `status` FROM `{}` WHERE trans_id = ?", tableName);

    /**
     * 获取所有事务记录
     */
    public static final ConsumerResult<String, String> SELECT_ALL_TRANSACTION_LOG =
            tableName -> StrKit.format("SELECT `trans_id`, `status`, `role`, `target_class`, `target_method`, `participants`, `version`, `create_time`, `last_time` FROM `{}`", tableName);


    /**
     * 根据事务id删除事务信息
     */
    public static final ConsumerResult<String, String> DELETE_TRANSACTION_BY_TRANS_ID_TEMPLATE =
            tableName -> StrKit.format("DELETE FROM `{}` WHERE `trans_id` = ?", tableName);


    /**
     * 根据事务id删除事务信息
     */
    public static final ConsumerResult<String, String> DELETE_TRANSACTION_COMPENSATIONS_LOG_BY_TRANS_ID_TEMPLATE =
            tableName -> StrKit.format("DELETE FROM `{}` WHERE `trans_id` = ?", tableName);


    /**
     * 事务乐观锁模板
     */
    public static final ConsumerResult<String, String> TRANSACTION_OPTIMISTIC_LOCK_TEMPLATE =
            tableName -> StrKit.format("UPDATE `{}` SET `version` = `version` + 1 , `last_time` = ?  WHERE `trans_id` = ? AND `version` = ? AND TIMESTAMPDIFF( SECOND, DATE_FORMAT( `last_time`, '%Y-%m-%d %H:%i:%s' ), DATE_FORMAT( now(), '%Y-%m-%d %H:%i:%s' )) >= ?", tableName);

    /**
     * 删除补偿记录
     */
    public static final ConsumerArgsResult<Object, String> DELETE_EXCESS_COMPENSATION_RECORD =
            tableNames -> StrKit.format("DELETE FROM `{}`  WHERE trans_id NOT IN ( SELECT trans_id FROM `{}` ) AND TIMESTAMPDIFF( SECOND, DATE_FORMAT( `create_time`, '%Y-%m-%d %H:%i:%s' ), DATE_FORMAT( now(), '%Y-%m-%d %H:%i:%s' )) >= ?", tableNames);

    /**
     * 生成 数据库插入语句模板
     *
     * @param tableName   数据库表名称
     * @param obj         表对应的数据模型
     * @param fieldValues 封装字段值的集合，注意必须为空集合
     * @param serializer  序列化对象
     * @return 插入语句模板
     * @throws IllegalAccessException 对象模型中某个字段不可访问
     */
    public static String insertSqlTemplate(String tableName, Object obj,
                                           List<Object> fieldValues, ObjectSerializer serializer)
            throws IllegalAccessException {
        Assert.notNull(tableName, "数据库对应数据表名称不可以为空");
        Assert.isTrue(CollectionKit.isNotEmpty(fieldValues), "封装字段值的集合必须为空");
        Assert.notNull(serializer, "序列化对象不可以为空");

        List<Field> fieldsList = ReflectKit.getAllFieldsList(obj.getClass());

        StringBuilder fieldBuilder = new StringBuilder();
        StringBuilder builder = new StringBuilder();
        for (Field field : fieldsList) {
            DbField dbField = field.getAnnotation(DbField.class);
            if (dbField == null) continue;

            Object fieldValue = ReflectKit.getFieldValue(obj, field.getName());
            if (fieldValue == null) continue;

            // 拼接字段参数
            fieldBuilder.append(",`").append(dbField.value()).append("`");
            builder.append(",").append("?");
            // 判断是否进行序列化
            if (dbField.serializer()) {
                fieldValue = serializer.serialize(fieldValue);
            }
            fieldValues.add(fieldValue);
        }

        Assert.isTrue(fieldBuilder.length() == 0, "未找到对象属性或未定义@DbField注解无法生成插入sql模板");

        fieldBuilder.delete(0, 1);
        builder.delete(0, 1);
        return StrKit.format("INSERT INTO `{}` ( {} ) VALUES ( {} )", tableName, fieldBuilder.toString(), builder.toString());
    }

}
