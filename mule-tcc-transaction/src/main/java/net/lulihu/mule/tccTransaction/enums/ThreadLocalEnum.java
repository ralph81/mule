package net.lulihu.mule.tccTransaction.enums;

/**
 * 线程副本枚举
 */
public enum ThreadLocalEnum {

    SERVLET_REQUEST("当前请求上下文"),

    TRANSACTION_CONTEXT("事务上下文"),

    TRANSACTION_LOG("事务记录"),

    ORDER_EXECUTE_LOCK("按序执行锁"),

    STARTER_TRANSACTION_EXECUTOR("事务发起者执行对象"),
    ;

    ThreadLocalEnum(String description) {
        this.description = description;
    }

    private final String description;

    public String getDescription() {
        return description;
    }
}
