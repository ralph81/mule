package net.lulihu.mule.tccTransaction.service.impl;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.ObjectKit.StrKit;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.MuleTccTransactionSelfHealingProgram;
import net.lulihu.mule.tccTransaction.eventExecutor.disruptor.DisruptorAsynchronousEventServiceImpl;
import net.lulihu.mule.tccTransaction.enums.RepositorySupportEnum;
import net.lulihu.mule.tccTransaction.enums.SerializeEnum;
import net.lulihu.mule.tccTransaction.exception.MuleTccException;
import net.lulihu.mule.tccTransaction.kit.ServiceLoaderKit;
import net.lulihu.mule.tccTransaction.serializer.KryoSerializer;
import net.lulihu.mule.tccTransaction.serializer.ObjectSerializer;
import net.lulihu.mule.tccTransaction.service.*;
import net.lulihu.mule.tccTransaction.service.coordinator.DefaultTransactionCoordinatorServiceImpl;
import net.lulihu.mule.tccTransaction.service.coordinator.db.DBTransactionCoordinatorRepositoryServiceImpl;
import net.lulihu.mule.tccTransaction.service.factory.TransactionFactoryComponent;

import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader;

@Slf4j
public class DefaultMuleTccBootServiceImpl implements MuleTccBootService {

    private final TransactionCoordinatorService transactionCoordinatorService;
    private final TransactionExecutorEventService transactionExecutorEventService;
    private final TransactionSelfHealingProgramService transactionSelfHealingProgramService;
    private final String applicationName;

    public DefaultMuleTccBootServiceImpl(String applicationName) {
        this(applicationName, new DefaultTransactionCoordinatorServiceImpl(),
                new DisruptorAsynchronousEventServiceImpl(), new MuleTccTransactionSelfHealingProgram());
    }

    public DefaultMuleTccBootServiceImpl(TransactionCoordinatorService transactionCoordinatorService) {
        this(transactionCoordinatorService, new DisruptorAsynchronousEventServiceImpl(), new MuleTccTransactionSelfHealingProgram());
    }

    public DefaultMuleTccBootServiceImpl(TransactionCoordinatorService transactionCoordinatorService,
                                         TransactionExecutorEventService transactionExecutorEventService,
                                         TransactionSelfHealingProgramService transactionSelfHealingProgramService) {
        this(null, transactionCoordinatorService, transactionExecutorEventService, transactionSelfHealingProgramService);
    }

    public DefaultMuleTccBootServiceImpl(String applicationName,
                                         TransactionCoordinatorService transactionCoordinatorService,
                                         TransactionExecutorEventService transactionExecutorEventService,
                                         TransactionSelfHealingProgramService transactionSelfHealingProgramService) {
        this.transactionCoordinatorService = transactionCoordinatorService;
        this.transactionExecutorEventService = transactionExecutorEventService;
        this.applicationName = applicationName;
        this.transactionSelfHealingProgramService = transactionSelfHealingProgramService;
        // 注册到组件工厂
        TransactionFactoryComponent.registered(transactionExecutorEventService);
        TransactionFactoryComponent.registered(transactionSelfHealingProgramService);
    }

    @Override
    public void initialization(MuleTccConfig config) throws Exception {
        if (StrKit.isBlank(config.getApplicationName())) {
            // 加载应用名称
            config.setApplicationName(Optional.ofNullable(applicationName()).orElseGet(() -> {
                throw new MuleTccException("未设置应用程序名称，将导致程序后续无法对应储存库");
            }));
        }

        // 加载储存库支持
        TransactionCoordinatorRepositoryService repositoryService = loadRepositorySupport(config);
        // 初始化事务协调器
        transactionCoordinatorService.initTransactionCoordinator(repositoryService, config);
        // 初始化事件执行者服务
        transactionExecutorEventService.initialization(config, transactionCoordinatorService);
        // 初始化自我恢复程序
        transactionSelfHealingProgramService.initialization(transactionCoordinatorService, config);
    }

    @Override
    public String applicationName() {
        return this.applicationName;
    }

    /**
     * 加载储存库支持
     *
     * @param config 配置
     */
    private TransactionCoordinatorRepositoryService loadRepositorySupport(MuleTccConfig config) {
        // 选择储存库
        TransactionCoordinatorRepositoryService repositoryService = selectCoordinatorRepository(config.getRepositorySupport());
        // 选择序列化对象
        ObjectSerializer objectSerializer = selectSerializedObject(config.getSerializer());
        // 储存库设置序列化对象
        repositoryService.setSerializer(objectSerializer);
        return repositoryService;
    }

    /**
     * 选择储存库序列化对象
     *
     * @param serializer 设置的序列化参数
     * @return 储存库序列化对象
     */
    private ObjectSerializer selectSerializedObject(String serializer) {
        SerializeEnum serializeEnum = SerializeEnum.acquire(serializer);

        ServiceLoader<ObjectSerializer> objectSerializers = ServiceLoaderKit.loadAll(ObjectSerializer.class);

        for (ObjectSerializer objectSerializer : objectSerializers) {
            if (Objects.equals(serializeEnum, objectSerializer.getScheme())) {
                LogKit.debug(log, "Mule Tcc 选取事务协调器序列化对象【{}】", () -> new Object[]{objectSerializer.getClass()});
                return objectSerializer;
            }
        }
        LogKit.warn(log, "错误的序列化参数设置【{}】，将使用kryo作为储存库序列化对象", serializer);
        return new KryoSerializer();
    }

    /**
     * 选择储存库对象
     *
     * @param repository 储存库参数
     * @return 储存库对象
     */
    private TransactionCoordinatorRepositoryService selectCoordinatorRepository(String repository) {
        RepositorySupportEnum repositorySupport = RepositorySupportEnum.acquire(repository);

        ServiceLoader<TransactionCoordinatorRepositoryService> repositoryServices =
                ServiceLoaderKit.loadAll(TransactionCoordinatorRepositoryService.class);

        for (TransactionCoordinatorRepositoryService repositoryService : repositoryServices) {
            if (Objects.equals(repositorySupport, repositoryService.getScheme())) {
                LogKit.debug(log, "Mule Tcc 选取事务协调器储存库对象【{}】", () -> new Object[]{repositoryService.getClass()});
                return repositoryService;
            }
        }
        LogKit.warn(log, "错误的储存库设置【{}】，将使用db作为储存库", repository);
        return new DBTransactionCoordinatorRepositoryServiceImpl();
    }

}
