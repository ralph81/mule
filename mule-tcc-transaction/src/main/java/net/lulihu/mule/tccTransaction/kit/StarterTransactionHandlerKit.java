package net.lulihu.mule.tccTransaction.kit;

import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.TransactionExecutorEventService;

/**
 * 事务发起者处理器方法工具
 */
public class StarterTransactionHandlerKit {

    /**
     * 执行确认方法
     *
     * @param transactionExecutorService 事务执行者
     */
    public static void exeConfirm(TransactionExecutorEventService transactionExecutorService) {
        if (transactionExecutorService == null) return;
        exe(MuleActionEnum.CONFIRMING, transactionExecutorService);
    }

    /**
     * 执行取消方法
     *
     * @param transactionExecutorService 事务执行者
     */
    public static void exeCancel(TransactionExecutorEventService transactionExecutorService) {
        if (transactionExecutorService == null) return;
        exe(MuleActionEnum.CANCELING, transactionExecutorService);
    }

    private static void exe(MuleActionEnum actionEnum, TransactionExecutorEventService transactionExecutorService) {
        if (transactionExecutorService == null) return;

        int code = actionEnum.getCode();
        MuleTransaction muleTransaction = TransactionLogLocalKit.get();
        muleTransaction.setStatus(code);
        TransactionContext transactionContext = TransactionContextLocalKit.get();
        transactionContext.setAction(code);
        transactionContext.setRole(MuleRoleEnum.CALLBACK_EXECUTION.getCode());

        if (actionEnum.equals(MuleActionEnum.CONFIRMING)) // 执行事务确认
            transactionExecutorService.confirm(muleTransaction, transactionContext);
        else if (actionEnum.equals(MuleActionEnum.CANCELING)) // 执行取消方法
            transactionExecutorService.cancel(muleTransaction, transactionContext);
    }

}
