package net.lulihu.mule.tccTransaction.annotation;

import java.lang.annotation.*;

/**
 * 开启tcc事务注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface MuleTcc {

    /**
     * 事务确认方法名称
     * <p>
     * 在全局执行成功时，调用目标方法
     *
     * @return 方法名称
     */
    String confirmMethod() default "";

    /**
     * 事务取消方法名称
     * <p>
     * 在全局执行过程中有一个失败时，调用目标方法，进行回滚
     *
     * @return 方法名称
     */
    String cancelMethod() default "";

    /**
     * 执行当前方法  默认为false
     * <p>
     * 注意: 当该方法为true时，confirmMethod()和cancelMethod()将失效
     * <p>
     * 场景:作用于远端rpc调用时，不设置确认或取消方法，将当前方法执行设为true
     */
    boolean currentMethod() default false;

    /**
     * 目标方法执行过程中发生例外时，该方法不参与回滚，默认为不参与
     * <p>
     * 注意:该属性只对RPC远端的方法生效
     *
     * @return true不参与反之回滚
     */
    boolean exceptionNotRollBack() default true;

}
