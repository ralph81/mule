package net.lulihu.mule.tccTransaction.service;

import net.lulihu.mule.tccTransaction.exception.NotFindSuitableObjectException;

import java.lang.reflect.Method;

/**
 * 对开启注解的事务方法 进行代理
 */
public interface TransactionMethodProxyService {

    /**
     * 在事务处理程序之前使用，获取事务处理器对象
     *
     * @return 处理当前请求的事务处理器对象
     */
    TransactionHandlerService beforeTransactionHandler() throws NotFindSuitableObjectException;

    /**
     * 目标方法 前置通知
     *
     * @param handlerService 事件处理器
     * @param beanClass      代理方法的目标对象类型
     * @param method         被代理的方法
     * @param args           目标方法参数
     * @return true继续执行目标方法反之不执行
     */
    boolean before(TransactionHandlerService handlerService, Class<?> beanClass, Method method, Object[] args);

    /**
     * 目标方法 结果通知
     *
     * @param handlerService 处理当前事务的事务处理器
     * @param result         目标方法的执行结果
     * @return 新的结果
     */
    Object result(TransactionHandlerService handlerService, Object result);

    /**
     * 目标方法 异常通知
     *
     * @param handlerService 处理当前事务的事务处理器
     * @param throwable      异常
     * @return 返回新的处理结果
     */
    Object exception(TransactionHandlerService handlerService, Throwable throwable) throws Throwable;

    /**
     * 目标方法 后置通知
     *
     * @param handlerService 处理当前事务的事务处理器
     */
    void after(TransactionHandlerService handlerService);

}
