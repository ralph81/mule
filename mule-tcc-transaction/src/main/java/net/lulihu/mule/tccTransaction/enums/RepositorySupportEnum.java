package net.lulihu.mule.tccTransaction.enums;


import java.util.Objects;

/**
 * 事务补偿存储类型枚举
 */
public enum RepositorySupportEnum {

    /**
     * Db补偿存储类型
     */
    DB("db"),
    ;

    RepositorySupportEnum(String support) {
        this.support = support;
    }

    private final String support;

    /**
     * 获取补偿存储类型枚举
     *
     * @param support 补偿存储类型
     * @return 补偿存储类型枚举
     */
    public static RepositorySupportEnum acquire(String support) {
        for (RepositorySupportEnum value : RepositorySupportEnum.values()) {
            if (Objects.equals(value.getSupport(), support)) {
                return value;
            }
        }
        return RepositorySupportEnum.DB;
    }

    public String getSupport() {
        return support;
    }
}
