package net.lulihu.mule.tccTransaction;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.dateTime.DateTime;
import net.lulihu.dateTime.DateTimeKit;
import net.lulihu.mule.tccTransaction.enums.MuleActionEnum;
import net.lulihu.mule.tccTransaction.enums.MuleRoleEnum;
import net.lulihu.mule.tccTransaction.kit.ParticipantKit;
import net.lulihu.mule.tccTransaction.kit.TransactionContextLocalKit;
import net.lulihu.mule.tccTransaction.model.MuleTransaction;
import net.lulihu.mule.tccTransaction.model.MuleTransactionCompensations;
import net.lulihu.mule.tccTransaction.model.TransactionContext;
import net.lulihu.mule.tccTransaction.service.MuleTccShutdownService;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;
import net.lulihu.mule.tccTransaction.service.TransactionSelfHealingProgramService;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 自我修复程序
 * <p>
 * 负责修复执行过程中回调执行失败的情况
 */
@Slf4j
public class MuleTccTransactionSelfHealingProgram
        implements Runnable, TransactionSelfHealingProgramService, MuleTccShutdownService {

    private TransactionCoordinatorService transactionCoordinatorService;

    private ScheduledExecutorService scheduledExecutorService;

    /**
     * 最大重试次数
     */
    private int retryMax;

    /**
     * 每次重试恢复时间间隔 单位/秒
     */
    private int recoverTimeInterval;

    /**
     * 首次恢复与创建时间的间隔 单位/毫秒
     */
    private long firstRecoveryTimeInterval;

    /**
     * 删除多余补偿记录的初始延迟间隔 单位/秒
     */
    private int deleteExcessCompensationRecordInitialDelay;

    @Override
    public void run() {
        LogKit.debug(log, "执行自我检查...");
        try {
            // 事务补偿
            transactionCompensate();
            // 删除多余事务补偿记录
            transactionCoordinatorService.deleteExcessCompensationRecord(deleteExcessCompensationRecordInitialDelay, null);
        } catch (Exception e) {
            LogKit.error(log, "执行自我检查时发生错误", e);
        }
    }

    /**
     * 事务补偿
     */
    private void transactionCompensate() {
        // 获取所有事务信息
        List<MuleTransaction> transactions = transactionCoordinatorService.getAllMuleTransaction(null);
        if (CollectionKit.isEmpty(transactions)) return;

        // 设置事物上下文
        TransactionContext context = new TransactionContext();
        context.setRole(MuleRoleEnum.SELF_HEALING.getCode());
        TransactionContextLocalKit.set(context);
        // 循环针对所有事务进行补偿
        for (MuleTransaction transaction : transactions) {
            int status = transaction.getStatus();

            // 首次恢复时间判断
            DateTime createTime = DateTimeKit.parse(transaction.getCreateTime(), DateTimeKit.NORM_DATETIME_MS_PATTERN);
            int compare = Long.compare(createTime.getTime() + firstRecoveryTimeInterval, DateTimeKit.date().getTime());
            if (compare > 0) continue;
            // 设置事物id
            context.setTransId(transaction.getTransId());

            // 如果为try执行之后则获取事件补偿记录尝试进行事务补偿
            if (status == MuleActionEnum.AFTER_TRYING.getCode()) {
                afterTrying(transaction);
                // 回调执行失败进行事务补偿
            } else if (status == MuleActionEnum.CONFIRMING.getCode() || status == MuleActionEnum.CANCELING.getCode()) {
                // 获取乐观锁
                boolean success = transactionCoordinatorService.getOptimisticLocks(transaction, recoverTimeInterval, null);
                if (success) {
                    // 执行事务方法
                    boolean transactionStatus = status == MuleActionEnum.CONFIRMING.getCode();
                    ParticipantKit.exeParticipantMethod(transactionCoordinatorService, transactionStatus, retryMax, transaction);
                }
            }
        }
    }

    /**
     * 状态为  {@link MuleActionEnum} MuleActionEnum.AFTER_TRYING 执行此方法自行恢复
     *
     * @param transaction 事务记录
     */
    private void afterTrying(MuleTransaction transaction) {
        MuleTransactionCompensations transactionCompensations = transactionCoordinatorService.getMuleTransactionCompensationsLog(transaction, null);
        if (transactionCompensations == null) return;

        // 获取乐观锁
        boolean success = transactionCoordinatorService.getOptimisticLocks(transaction, recoverTimeInterval, null);
        if (success) {
            // 删除补偿记录
            transaction.setStatus(transactionCompensations.getStatus());
            transactionCoordinatorService.deleteTransactionCompensationsLog(transactionCompensations, null);
            // 删除事务记录
            if (MuleActionEnum.DELETE.getCode() == transactionCompensations.getStatus()) {
                transactionCoordinatorService.delete(transaction, null);
            } else {  // 执行事务方法
                boolean transactionStatus = transaction.getStatus() == MuleActionEnum.CONFIRMING.getCode();
                ParticipantKit.exeParticipantMethod(transactionCoordinatorService, transactionStatus, retryMax, transaction);
            }
        }
    }


    /**
     * 初始化自我恢复程序
     *
     * @param transactionCoordinatorService 事务协调员服务
     * @param config                        配置
     */
    @Override
    public void initialization(TransactionCoordinatorService transactionCoordinatorService, MuleTccConfig config) {
        LogKit.debug(log, "初始化自我修复程序...");

        this.transactionCoordinatorService = transactionCoordinatorService;
        this.retryMax = config.getRetryMax();
        this.recoverTimeInterval = config.getRecoverTimeInterval();
        this.firstRecoveryTimeInterval = TimeUnit.SECONDS.toMillis(config.getFirstRecoveryTimeInterval());
        this.deleteExcessCompensationRecordInitialDelay = config.getDeleteExcessCompensationRecordInitialDelay();

        // 设置线程工厂
        AtomicInteger atomic = new AtomicInteger();
        ThreadFactory threadFactory = thread -> new Thread(new ThreadGroup("Mule-SelfHealingProgram"), thread,
                "Mule自我检查程序-" + atomic.incrementAndGet());
        this.scheduledExecutorService = new ScheduledThreadPoolExecutor(config.getScheduledThreadMax(), threadFactory);
        this.scheduledExecutorService.scheduleWithFixedDelay(this, config.getScheduledInitDelay(), config.getScheduledDelay(), TimeUnit.SECONDS);

        // 注册到关闭处理程序中
        MuleTccShutdownManage.getInstance().addComponents(this);
    }

    @Override
    public void shutdown() {
        LogKit.debug(log, "自我检查程序开始关闭...");
        LogKit.debug(log, "自我检查程序停止生产新的线程...");

        this.scheduledExecutorService.shutdown();
        boolean stop = false;
        do {
            try {
                stop = this.scheduledExecutorService.awaitTermination(2, TimeUnit.SECONDS);
                LogKit.debug(log, "自我检查程序关闭监听 - {}...", stop ? "关闭成功" : "任务还在执行中，请等待");
            } catch (InterruptedException e) {
                if (Thread.interrupted()) {
                    LogKit.warn(log, "自我检查程序关闭线程等待过程中被打断...");
                }
            }
        } while (!stop);
    }

    @Override
    public int order() {
        return 1;
    }
}
