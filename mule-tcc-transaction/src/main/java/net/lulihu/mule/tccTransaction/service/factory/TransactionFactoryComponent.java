package net.lulihu.mule.tccTransaction.service.factory;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.CollectionKit;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.mule.tccTransaction.exception.FactoryCompontClassCastException;
import net.lulihu.mule.tccTransaction.kit.ServiceLoaderKit;
import net.lulihu.mule.tccTransaction.service.TransactionSupportService;

import java.util.*;

/**
 * 事务组件工厂
 * <p>
 * 负责将组件的进和出
 */
@Slf4j
@SuppressWarnings("unchecked")
public class TransactionFactoryComponent {

    /**
     * 私有化构造函数
     */
    private TransactionFactoryComponent() {
    }

    /**
     * 启动工厂 ，为各个车间添加组件
     */
    public static void startFactory() throws Exception {
        LogKit.debug(log, "开始注册 Mule Tcc 组件...");

        // 循环注册所有定义的组件
        for (FactoryComponentEnum workshopSet : FactoryComponentEnum.values()) {
            // 获取注册的处理器程序
            ServiceLoader<?> handlerServices = ServiceLoaderKit.loadAll(workshopSet.getClazz());
            addAllComponentService(handlerServices);
        }

        // 初始化成功，执行加载完成回调
        for (FactoryComponentEnum workshopSet : FactoryComponentEnum.values()) {
            Collection<TransactionFactoryService> workshop = (Collection<TransactionFactoryService>) workshopSet.getWorkshop();

            // 排序 按照order 方法返回值排序
            workshop.stream().sorted(Comparator.comparingInt(TransactionSupportService::order));

            for (TransactionFactoryService component : workshop) {
                component.afterInitialization();
            }
        }
    }


    /**
     * 添加组件
     * <p>
     * 注意 该方法允许用户自定义组件进行拓展，
     * 但是为避免自定义组件 {@link TransactionFactoryService}的 afterInitialization 方法不执行，
     * 请在 startFactory 方法调用前进行注册，或者自行调用 afterInitialization 方法
     *
     * @param factoryService 组件
     */
    public static void registered(TransactionFactoryService factoryService) {
        FactoryComponentEnum componentSet = FactoryComponentEnum.getComponentSetByClazz(factoryService.getClass());
        Collection<TransactionFactoryService> factoryServiceSet = (Collection<TransactionFactoryService>) componentSet.getWorkshop();
        factoryServiceSet.add(factoryService);

        // 插入组件重新排序
        factoryServiceSet.stream().sorted(Comparator.comparingInt(TransactionSupportService::order));
        LogKit.debug(log, "组件【{}】成功注册至Mule Tcc组件工厂...", () -> new Object[]{factoryService.componentName()});
    }

    /**
     * 批量添加组件
     *
     * @param handlerServices 处理器组件
     */
    private static void addAllComponentService(ServiceLoader<?> handlerServices) {
        if (CollectionKit.isEmpty(handlerServices)) return;
        // 类型判断
        Iterator<?> iterator = handlerServices.iterator();
        Object next = iterator.next();
        if (!(next instanceof TransactionFactoryService))
            throw new FactoryCompontClassCastException("工厂内的组件都必须实现 TransactionFactoryService 接口作为标识");

        // 寻找组件对应的生产车间
        FactoryComponentEnum componentSet = FactoryComponentEnum.getComponentSetByClazz(next.getClass());
        Collection<TransactionFactoryService> factoryServiceSet = (Collection<TransactionFactoryService>) componentSet.getWorkshop();

        TransactionFactoryService component = (TransactionFactoryService) next;
        factoryServiceSet.add(component);
        LogKit.debug(log, "组件【{}】注册成功", () -> new Object[]{component.componentName()});

        // 循环添加
        Iterator<TransactionFactoryService> serviceIterator = (Iterator<TransactionFactoryService>) iterator;
        while (serviceIterator.hasNext()) {
            TransactionFactoryService componentService = serviceIterator.next();
            factoryServiceSet.add(componentService);
            LogKit.debug(log, "组件【{}】注册成功", () -> new Object[]{componentService.componentName()});
        }

    }

    /**
     * 根据组件类型对应车间生产的所有组件
     *
     * @return 组件类型
     */
    public static <T> Collection<T> getWorkshopComponentSetByClazz(Class<T> clazz) {
        return (Collection<T>) FactoryComponentEnum.getComponentSetByClazz(clazz).getWorkshop();
    }

}
