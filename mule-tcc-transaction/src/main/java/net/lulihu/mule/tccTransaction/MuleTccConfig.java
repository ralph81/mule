package net.lulihu.mule.tccTransaction;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.lulihu.mule.tccTransaction.enums.RepositorySupportEnum;
import net.lulihu.mule.tccTransaction.enums.SerializeEnum;

import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Data
@NoArgsConstructor
public class MuleTccConfig {

    /**
     * 应用程序名称 （必须设置否则将导致启动失败）
     */
    private String applicationName;

    /**
     * 序列化程序。
     * {@link  SerializeEnum}
     */
    private String serializer = "kryo";

    /**
     * 自我修复程序最大线程数
     * <p>
     * 默认超过4核的处理器为两个线程执行，反之一个线程
     */
    private int scheduledThreadMax = Runtime.getRuntime().availableProcessors() > 4 ? 2 : 1;

    /**
     * 自我修复计划修复查询间隔 单位/秒
     */
    private int scheduledDelay = 30;

    /**
     * 自我修复计划初始执行延迟时间 单位/秒
     */
    private int scheduledInitDelay = 5;

    /**
     * 事务提交或取消最多重试次数
     */
    private int retryMax = 3;

    /**
     * 重试时间最小间隔 单位秒
     * <p>
     * 既重试3次每次重试的时间间隔，真实重试时间大于等于此属性的时间
     */
    private int recoverTimeInterval = 5;

    /**
     * 事务首次恢复延迟时间 单位/秒
     */
    private int firstRecoveryTimeInterval = 15;

    /**
     * 删除多余补偿记录的初始延迟时间 单位/秒
     * <p>
     * 例: rpc调用时，被调用对象掉线导致调用对象发生错误，调用对象执行自我修复时会先被调用对象发送补偿
     * 请求，但是因为被调用对象因为掉线没有事务记录，所以该补偿记录将一直找不到事务记录，这边统一做一个删除
     */
    private int deleteExcessCompensationRecordInitialDelay = 30;

    /**
     * 事务记录保存存储库支持
     * {@link  RepositorySupportEnum}
     */
    private String repositorySupport = "db";

    /**
     * 异步线程池 缓冲区大小
     */
    private int bufferSize = 2048;

    /**
     * 异步线程池 消费者线程数量
     */
    private int consumerThreadNum = Runtime.getRuntime().availableProcessors() << 1;

    /**
     * 数据库存储配置
     */
    private DbConfig dbConfig = new DbConfig();

    /**
     * 数据库存储配置
     */
    @Data
    @NoArgsConstructor
    public static class DbConfig {

        /**
         * 驱动程序全类名
         */
        private String driverClassName = "com.mysql.jdbc.Driver";

        /**
         * 数据库地址
         */
        private String url;

        /**
         * 账号
         */
        private String username;

        /**
         * 密码
         */
        private String password;


        /**
         * 最大活跃连接
         * <p>
         * https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing
         */
        private int maxActive = 20;

        /**
         * 最小活跃连接
         */
        private int minIdle = 10;


        /**
         * 此属性控制数据库连接池中连接的最大超时毫秒数。
         * 如果在没有连接可用的情况下超过此时间，将抛出SQLException。
         * 最低可接受连接超时为250毫秒。
         * 默认值：10秒
         */
        private long connectionTimeout = TimeUnit.SECONDS.toMillis(10);


        /**
         * 此属性控制允许连接在连接池中最长空闲时间。
         * 此设置仅在minimumIdle定义为小于maximumPoolSize时适用。
         * 一旦池达到minimumIdle连接，空闲连接将不会退出。
         * 连接是否空闲，最大变化为+30秒，
         * 和平均变化+15秒。 在此超时之前，连接永远不会被空闲。
         * 值为0表示永远不会从池中删除空闲连接。 允许的最小值为10000毫秒（10秒）。
         * 默认值：600000（10分钟）
         */
        private long idleTimeout = TimeUnit.MINUTES.toMillis(10);


        /**
         * 此属性控制连接池中连接的最长生命周期。 永远不会使用正在使用的连接退休， 只有当它关闭时才会被删除。
         * 在逐个连接的基础上，轻微的衰减以避免池中的大量灭绝。
         * 我们强烈建议您设置此值应比任何数据库或基础设施施加的连接时间限制短几秒。
         * 0表示没有最大寿命（无限寿命）
         * 默认值：1800000（30分钟）
         */
        private long maxLifetime = TimeUnit.MINUTES.toMillis(30);


        /**
         * 如果您的驱动程序支持JDBC4，我们强烈建议您不要设置此属性。
         * 这适用于不支持JDBC4 Connection.isValid（）API的“遗留”驱动程序。
         * 这是在从池中给出连接之前执行的查询，以验证与数据库的连接是否仍然存在。
         * 再次尝试运行没有此属性的池，如果您的驱动程序不符合JDBC4，
         * HikariCP将记录错误以通知您。 默认值：无
         */
        private String connectionTestQuery;

        /**
         * 添加将用于配置多数据源  key->数据源名称  value-> DataSource/java.sql.Driver的属性 。
         */
        private Map<String, Object> dataSourcePropertyMap;

        /**
         * 您可以使用现有DataSource或根据配置生成新的DataSource。
         */
        private DataSource dataSource;
    }

}
