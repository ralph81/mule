package net.lulihu.mule.tccTransaction;

import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.mule.tccTransaction.service.MuleTccShutdownService;

import java.util.*;

/**
 * 应用程序关闭管理
 */
@Slf4j
public class MuleTccShutdownManage {

    /**
     * 构造函数私有化
     */
    private MuleTccShutdownManage() {
        this.orderComponents = new TreeMap<>(Comparator.naturalOrder());
        this.randomComponents = new ArrayList<>();
    }

    /**
     * 获取实例
     *
     * @return 返回MuleTccShutdownManage实例对象
     */
    public static MuleTccShutdownManage getInstance() {
        return MuleTccShutdownManageEnum.INSTANCE.getMuleTccShutdownManage();
    }

    /**
     * 需要按照顺序关闭的组件 对应的树形结构
     * <p>
     * key->组件关闭排序数值   value->组件对象
     */
    private final Map<Integer, List<MuleTccShutdownService>> orderComponents;

    /**
     * 对什么时候关闭没有顺序的组件，这些组件会在需要按照顺序关闭的组件关闭后执行
     */
    private final List<MuleTccShutdownService> randomComponents;

    /**
     * 添加组件
     * <p>
     * 如果 order未负数 则添加至 randomComponents,为避免出现相同的order值组件丢失，
     * 使用队列承载，关闭时按照插入队列的顺序关闭<br>
     * 反之添加至  components
     *
     * @param shutdownService 组件对象
     */
    public synchronized void addComponents(MuleTccShutdownService shutdownService) {
        int order = shutdownService.order();
        if (order > -1) orderComponents.computeIfAbsent(order, k -> new ArrayList<>()).add(shutdownService);
        else randomComponents.add(shutdownService);

        LogKit.debug(log, "【{}】成功添加到Mule Tcc组件关闭系统...", () -> new Object[]{shutdownService.componentName()});
    }


    synchronized void startShutdown() {
        LogKit.debug(log, "Mule Tcc 开始逐步关闭组件...");

        // 先关闭需要按照顺序的组件
        Map<Integer, List<MuleTccShutdownService>> orderComponents = this.orderComponents;
        for (Integer order : orderComponents.keySet()) {
            List<MuleTccShutdownService> shutdownServices = orderComponents.get(order);
            for (MuleTccShutdownService shutdownService : shutdownServices) {
                shutdown(shutdownService);
            }
        }

        // 最后关闭其他组件
        List<MuleTccShutdownService> randomComponents = this.randomComponents;
        for (MuleTccShutdownService randomComponent : randomComponents) {
            shutdown(randomComponent);
        }

        LogKit.debug(log, "Mule Tcc 组件全部关闭...");
    }

    private void shutdown(MuleTccShutdownService shutdownService) {
        String componentName = shutdownService.componentName();
        try {
            LogKit.debug(log, "组件对象【{}】开始关闭...", componentName);

            shutdownService.shutdown();
        } catch (Exception e) {
            LogKit.error(log, "关闭组件对象【{}】时发生例外", componentName, e);
        }
    }

    /**
     * 利用枚举创建单例模式
     */
    private enum MuleTccShutdownManageEnum {
        INSTANCE;

        MuleTccShutdownManageEnum() {
            this.muleTccShutdownManage = new MuleTccShutdownManage();
        }

        private MuleTccShutdownManage muleTccShutdownManage;

        public MuleTccShutdownManage getMuleTccShutdownManage() {
            return muleTccShutdownManage;
        }
    }

}
