package net.lulihu.mule.tccTransaction.exception;

import net.lulihu.ObjectKit.StrKit;

/**
 * 参与者方法执行失败例外
 */
public class ParticipantMethodExeFailureException extends RuntimeException {

    public ParticipantMethodExeFailureException() {
        super();
    }

    public ParticipantMethodExeFailureException(String message) {
        super(message);
    }

    public ParticipantMethodExeFailureException(String message, Object... values) {
        super(StrKit.format(message, values));
    }

    public ParticipantMethodExeFailureException(Throwable cause, String message, Object... values) {
        super(StrKit.format(message, values), cause);
    }

    public ParticipantMethodExeFailureException(Throwable cause) {
        super(cause);
    }

    public ParticipantMethodExeFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
