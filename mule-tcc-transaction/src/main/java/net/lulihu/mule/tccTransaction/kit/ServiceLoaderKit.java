package net.lulihu.mule.tccTransaction.kit;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * 服务装载工具
 */
public class ServiceLoaderKit {

    /**
     * 获取首个加载器
     *
     * @param clazz 参数类型
     * @return 该类型对象
     */
    public static <S> S loadFirst(final Class<S> clazz) {
        ServiceLoader<S> loader = loadAll(clazz);
        Iterator<S> iterator = loader.iterator();
        if (!iterator.hasNext()) {
            throw new IllegalStateException("在/META-INF/services/" + clazz.getName() + "中没有定义任何实现，" +
                    "请检查文件是否存在并且具有正确的实现类！");
        }
        return iterator.next();
    }

    /**
     * 获取指定类型的对象加载器。
     *
     * @param clazz 参数类型
     * @return 对象加载器
     */
    public static <S> ServiceLoader<S> loadAll(final Class<S> clazz) {
        return ServiceLoader.load(clazz);
    }


}
