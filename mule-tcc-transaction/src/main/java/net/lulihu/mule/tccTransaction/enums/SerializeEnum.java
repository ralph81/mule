
package net.lulihu.mule.tccTransaction.enums;

import java.util.Objects;

/**
 * 序列化枚举
 */
public enum SerializeEnum {

    /**
     * Jdk序列化协议
     */
    JDK("jdk"),

    /**
     * Kryo序列化协议
     */
    KRYO("kryo");


    SerializeEnum(String serialize) {
        this.serialize = serialize;
    }

    private final String serialize;

    /**
     * 获取序列化协议序列化协议
     *
     * @param serialize 序列化协议
     */
    public static SerializeEnum acquire(final String serialize) {
        for (SerializeEnum value : SerializeEnum.values()) {
            if (Objects.equals(value.getSerialize(), serialize)) {
                return value;
            }
        }
        return SerializeEnum.KRYO;
    }

    public String getSerialize() {
        return serialize;
    }
}
