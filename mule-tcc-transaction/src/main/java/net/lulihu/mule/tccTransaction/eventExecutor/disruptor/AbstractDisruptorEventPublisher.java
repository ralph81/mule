package net.lulihu.mule.tccTransaction.eventExecutor.disruptor;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.ExceptionHandler;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import lombok.extern.slf4j.Slf4j;
import net.lulihu.ObjectKit.LogKit;
import net.lulihu.ObjectKit.NumberKit;
import net.lulihu.disruptorKit.DefaultEventFactory;
import net.lulihu.disruptorKit.Event;
import net.lulihu.disruptorKit.Producer;
import net.lulihu.mule.tccTransaction.MuleTccConfig;
import net.lulihu.mule.tccTransaction.MuleTccShutdownManage;
import net.lulihu.mule.tccTransaction.service.MuleTccShutdownService;
import net.lulihu.mule.tccTransaction.service.TransactionCoordinatorService;
import net.lulihu.mule.tccTransaction.service.TransactionExecutorEventService;
import net.lulihu.mule.tccTransaction.service.TransactionHandlerService;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * 使用 Disruptor 实现 异常队列处理
 */
@Slf4j
public abstract class AbstractDisruptorEventPublisher implements TransactionExecutorEventService,
        MuleTccShutdownService, ExceptionHandler<Event<DisruptorTransactionEvent>> {

    private Disruptor<Event<DisruptorTransactionEvent>> disruptor;

    // 生产者对象
    private Producer<DisruptorTransactionEvent> producer;

    protected TransactionCoordinatorService transactionCoordinatorService;

    @Override
    public final void initialization(MuleTccConfig config, TransactionCoordinatorService transactionCoordinatorService) {
        this.transactionCoordinatorService = transactionCoordinatorService;

        // 初始化disruptor队列
        LogKit.debug(log, "开始初始化disruptor异步队列...");
        disruptorInitialization(config);
        LogKit.debug(log, "disruptor异步队列初始化成功...");

        // 注册至关闭程序
        MuleTccShutdownManage.getInstance().addComponents(this);
    }

    /**
     * 初始化 disruptor队列
     *
     * @param config 配置
     */
    private void disruptorInitialization(MuleTccConfig config) {
        int bufferSize = config.getBufferSize();
        bufferSize = Integer.bitCount(bufferSize) == 1 ? bufferSize : NumberKit.getClosest2IndexGreaterThanSelf(bufferSize);
        // 创建消费者对象
        int consumerThreadNum = config.getConsumerThreadNum();
        DisruptorEventConsumer[] consumers = new DisruptorEventConsumer[consumerThreadNum];
        for (int i = 0; i < consumerThreadNum; i++) {
            consumers[i] = new DisruptorEventConsumer(transactionCoordinatorService);
        }
        // 设置线程工厂
        AtomicInteger atomic = new AtomicInteger();
        ThreadFactory threadFactory = thread -> new Thread(new ThreadGroup("Mule-disruptor"), thread,
                "Mule异步执行线程-" + atomic.incrementAndGet());
        // 创建 disruptor
        disruptor = new Disruptor<>(DefaultEventFactory.factory(), bufferSize, threadFactory,
                ProducerType.MULTI, new BlockingWaitStrategy());
        // 设置消费者
        disruptor.handleEventsWithWorkerPool(consumers);
        // 设置默认异常处理
        disruptor.setDefaultExceptionHandler(this);
        // 获取生产者对象
        producer = new Producer<>(disruptor.getRingBuffer());
        // 启动
        disruptor.start();
    }

    /**
     * 提交至队列执行
     *
     * @param eventType 事件类型
     * @param args      事件方法参数
     */
    protected final void submit(EventTypeEnum eventType, Object... args) {
        DisruptorTransactionEvent transactionEvent = new DisruptorTransactionEvent();
        transactionEvent.setEventType(eventType);
        transactionEvent.setArgs(args);
        producer.submit(transactionEvent);
    }

    @Override
    public void handleOnStartException(Throwable ex) {
        LogKit.error(log, "Disruptor 启动时出现例外...", ex);
    }

    @Override
    public void handleEventException(Throwable ex, long sequence, Event<DisruptorTransactionEvent> event) {
        LogKit.error(log, "Disruptor 异步线程处理过程中发生错误...\n处理事件参数:{}", event, ex);
    }

    @Override
    public void handleOnShutdownException(Throwable ex) {
        LogKit.error(log, "Disruptor 关闭时出现例外...", ex);
    }

    // 暂时写死为true 默认所有事件都处理
    @Override
    public boolean support(TransactionHandlerService obj) {
        return true;
    }

    @Override
    public int order() {
        return 0;
    }

    @Override
    public String componentName() {
        return "Disruptor 异步线程队列";
    }

    @Override
    public final void shutdown() {
        LogKit.debug(log, "Disruptor 停止接收新的消息...");
        LogKit.debug(log, "Disruptor 开始处理内部剩余消息...");

        this.disruptor.shutdown();

        LogKit.debug(log, "Disruptor 关闭成功...");
    }
}
