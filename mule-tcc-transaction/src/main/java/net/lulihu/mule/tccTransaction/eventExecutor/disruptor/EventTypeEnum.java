package net.lulihu.mule.tccTransaction.eventExecutor.disruptor;

import lombok.ToString;

/**
 * 异步事件枚举
 * <p>
 * 负责告诉异步线程执行协调器的那个方法
 */
@ToString
public enum EventTypeEnum {

    SAVE("save", "保存事务记录"),
    DELETE("delete", "删除事务记录"),
    DELETE_BY_CONTEXT("deleteByContext", "删除事务记录"),
    UPDATE_PARTICIPANT("updateParticipant", "更新事务参与者"),
    UPDATE_STATUS("updateStatus", "修改事务状态"),
    CANCEL("cancel", "执行失败，进行事务补偿"),
    CONFIRM("confirm", "执行成功，进行事务确认"),
    RPC_CANCEL("rpcCancel", "执行失败，通知参与者进行事务补偿"),
    RPC_CONFIRM("rpcConfirm", "执行成功，通知参与者进行事务确认"),
    ;

    EventTypeEnum(String methodName, String description) {
        this.methodName = methodName;
        this.description = description;
    }

    private final String methodName;
    private final String description;

    public String getMethodName() {
        return methodName;
    }

    public String getDescription() {
        return description;
    }
}
